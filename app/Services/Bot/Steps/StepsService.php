<?php

namespace App\Services\Bot\Steps;

use Illuminate\Support\Facades\Cache;

class StepsService
{

    const START = 0;
    const TITLE = 1;
    const TEXT  = 2;
    const IMAGE = 3;
    const CONFIRM = 4;

    protected const STEP_STR = "tg_user_step#";

    public function getUserStep($tgId)
    {
        return Cache::remember(self::getStepString($tgId), now()->addMinutes(10), fn() => 0);
    }

    public function toStep($tgId, $step)
    {
        return Cache::put(self::getStepString($tgId), $step, now()->addMinutes(10));
    }

    public function nextStep($tgId)
    {
        return Cache::increment(self::getStepString($tgId));
    }

    private static function getStepString(int $tgId)
    {
        return self::STEP_STR.$tgId;
    }
}
