<?php

namespace App\Services\Bot\Steps\Bases;

use App\Services\Bot\Steps\StepsService;
use Illuminate\Support\Facades\Cache;

abstract class StepHandler
{
    protected static string $cacheName = 'middle_cache_';

    public abstract function clear();

    public function confirm(StepsService $stepsService, $tgId)
    {
        if(method_exists($this, 'nextStep')) {
            return $stepsService->toStep($tgId, $this->nextStep());
        }

        return $stepsService->nextStep($tgId);
    }

    protected string $stepName;
    protected function middleStepsResolve($tgId)
    {
        $step = Cache::remember($this->getCacheName($tgId), now()->addMinutes(10), fn() => 0);

    }

    protected function getCacheName($tgId)
    {
        return self::$cacheName.$this->stepName.'#'.$tgId;
    }
}
