<?php

namespace App\Services\Bot\Handlers;

use App\Services\Bot\Resolvers\StepsResolver;
use App\Services\Bot\Steps\StepsService;

class WebhookHandler
{

    /**
     * @var StepsService
     */
    protected StepsService $stepsService;

    /**
     * @var StepsResolver
     */
    protected StepsResolver $stepsResolver;

    public function __construct(
        StepsService $stepsService,
        StepsResolver $stepsResolver
    )
    {
        $this->stepsService = $stepsService;
        $this->stepsResolver = $stepsResolver;
    }

    public function handle()
    {
        $updates = \Telegram::getWebhookUpdates();
        \Log::info('#update: '.$updates);
        $telegram = $updates['message'];

        $tgId = $telegram['from']['id'];
        \Log::info('#tg_id: '.$tgId);

        $step = $this->stepsService->getUserStep($tgId);
        \Log::info('#step: '.$step);
        $this->stepsResolver->resolve($step);
    }
}
