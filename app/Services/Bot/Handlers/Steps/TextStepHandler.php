<?php

namespace App\Services\Bot\Handlers\Steps;

use App\Services\Bot\Steps\StepsService;
use App\Services\Idea\IdeaService;
use Illuminate\Support\Facades\Cache;

class TextStepHandler
{
    public function handle()
    {
        try {
        $stepsService = app()->make(StepsService::class);
        $updates = \Telegram::getWebhookUpdates();
        $telegram = $updates['message'];

        $tgId = $telegram['from']['id'];
        $text = $telegram['text'];
        Cache::put('text#'.$tgId, $text, now()->addMinutes(30));

        \Telegram::sendMessage([
           'chat_id' => $tgId,
           'text' => "Отлично! Я думаю это крутая идея, чтобы там не было :\n{$text}"
         ]);
            \Telegram::sendMessage([
               'chat_id' => $tgId,
               'text' => "Твоя идея отправлена!"
           ]);

        $ideaService = app()->make(IdeaService::class);



        $ideaService->create([
            'title' => Cache::pull('title#'.$tgId),
            'text'  => Cache::pull('text#'.$tgId),
            'user_id' => $tgId
                             ]);

        $stepsService->toStep($tgId, 0);

        } catch (\Exception $e) {
            return response('ok');
        }
        response('ok');
    }
}
