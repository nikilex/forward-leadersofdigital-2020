<?php

namespace App\Services\Bot\Handlers\Steps;


use App\Services\Bot\Steps\StepsService;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartStepHandler
{
    public function handle()
    {
            $stepsService = app()->make(StepsService::class);
            $updates = \Telegram::getWebhookUpdates();
            $telegram = $updates['message'];

            $tgId = $telegram['from']['id'];

            \Telegram::sendMessage([
               'chat_id' => $tgId,
               'text' => "Привет! Я твой приятель бот! Я знаю, ты сейчас расскажешь мне крутую идею! **Давай начнем с заголовка**\rПришли его в ответ на это сообщение:"
            ]);

            $stepsService->nextStep($tgId);
    }
}
