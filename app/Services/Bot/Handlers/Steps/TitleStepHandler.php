<?php

namespace App\Services\Bot\Handlers\Steps;

use App\Services\Bot\Steps\StepsService;
use Illuminate\Support\Facades\Cache;

class TitleStepHandler
{
    public function handle()
    {
        try {
            $stepsService = app()->make(StepsService::class);
            $updates      = \Telegram::getWebhookUpdates();
            $telegram     = $updates['message'];

            $tgId = $telegram['from']['id'];
            $text = $telegram['text'];

            Cache::put('title#' . $tgId, $text, now()->addMinutes(30));

            \Telegram::sendMessage(
                [
                    'chat_id' => $tgId,
                    'text'    => "Отлично! В заголовках всегда вся суть:\n**{$text}**"
                ]
            );

            \Telegram::sendMessage(
                [
                    'chat_id' => $tgId,
                    'text'    => "Теперь давай распишем идею подробнее, отправляй всё одним сообщением"
                ]
            );

            $stepsService->nextStep($tgId);
        } catch (\Exception $e) {
            return response('ok');
        }
        response('ok');

    }
}
