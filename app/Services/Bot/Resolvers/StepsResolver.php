<?php

namespace App\Services\Bot\Resolvers;

use App\Services\Bot\Handlers\Steps\ConfirmStepHandler;
use App\Services\Bot\Handlers\Steps\StartStepHandler;
use App\Services\Bot\Handlers\Steps\TextStepHandler;
use App\Services\Bot\Handlers\Steps\TitleStepHandler;
use App\Services\Bot\Steps\StepsService;

class StepsResolver
{
    protected function handlers()
    {
        return [
            StepsService::START    => StartStepHandler::class,
            StepsService::TITLE    => TitleStepHandler::class,
            StepsService::TEXT     => TextStepHandler::class,
            StepsService::IMAGE    => TextStepHandler::class,
            StepsService::CONFIRM  => ConfirmStepHandler::class
        ];
    }

    public function resolve($step)
    {
        app()->make(data_get($this->handlers(), $step))->handle();
    }
}
