<?php

namespace App\Services\Bot;

use App\Services\Bot\Handlers\WebhookHandler;
use App\Services\Bot\Steps\StepsService;

class BotService
{
    /**
     * @var WebhookHandler
     */
    protected WebhookHandler $webhookHandle;

    public function __construct(
        WebhookHandler $webhookHandler
    ) {
        $this->webhookHandle = $webhookHandler;
    }

    public function webhook()
    {
        $this->webhookHandle->handle();
    }
}
