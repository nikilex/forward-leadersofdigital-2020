<?php

namespace App\Services\Idea\Tag;

use App\Models\Tag;
use App\Services\Idea\Tag\Handlers\CreateTagHandler;
use App\Services\Idea\Tag\Handlers\FindByNameHandler;
use Illuminate\Support\Facades\Auth;

class TagService
{

    protected CreateTagHandler $createHandler;

    public function __construct(
        CreateTagHandler $createHandler,
        FindByNameHandler $byNameHandler
    ) {
        $this->createHandler = $createHandler;
        $this->byNameHandler = $byNameHandler;
    }

    public function create(array $data)
    {
        return $this->createHandler->handle($data);
    }

    public function findById(int $tagId)
    {
        return Tag::findOrFail($tagId);
    }

    public function findByName(string $tagName)
    {
        return $this->byNameHandler->handle($tagName);
    }

    public function getPaginated()
    {
        return Tag::orderBy('slug', 'desc')->paginate();
    }

    public function update(int $tagId, array $data)
    {
        $tag = $this->findById($tagId);
        $tag->update([
            'name'        =>    $data['name'],
            'modified_by'  =>    Auth::user()->id
         ]);

        return $tag;
    }

    public function remove(int $tagId)
    {
        $tag = $this->findById($tagId);
        return $tag->delete();
    }


}
