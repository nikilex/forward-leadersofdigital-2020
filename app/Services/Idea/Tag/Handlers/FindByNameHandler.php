<?php

namespace App\Services\Idea\Tag\Handlers;

use App\Models\Tag;

class FindByNameHandler
{
    public function handle(string $name)
    {
        return Tag::where('name', 'like', "%{$name}%")->get();
    }
}
