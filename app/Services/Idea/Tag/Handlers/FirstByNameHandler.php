<?php

namespace App\Services\Idea\Tag\Handlers;

use App\Models\Tag;

class FirstByNameHandler
{
    public function handle(string $name)
    {
        return Tag::where('name', '=', $name)->first();
    }
}
