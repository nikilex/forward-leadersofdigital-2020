<?php

namespace App\Services\Idea\Tag\Handlers;

use App\Models\Tag;
use Illuminate\Support\Facades\Auth;

class CreateTagHandler
{

    /**
     * @var FirstByNameHandler
     */
    protected FirstByNameHandler $firstByNameHandler;

    public function __construct(FirstByNameHandler $firstByNameHandler)
    {
        $this->firstByNameHandler = $firstByNameHandler;
    }

    public function handle(array $data)
    {
        $tagByName = $this->firstByNameHandler->handle($data['name']);

        if (!$tagByName) {
            return Tag::create([
                                   'name'         =>    $data['name'],
                                   'created_by'   =>    Auth::user()->id,
                                   'published'    =>    1
                               ]);
        }

        return  $tagByName;
    }
}
