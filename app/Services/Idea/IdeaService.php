<?php

namespace App\Services\Idea;

use App\Models\Idea;
use App\User;
use Cog\Laravel\Love\ReactionType\Models\ReactionType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class IdeaService
{
//    public function __construct()
//    {
//
//    }

    public function find(int $ideaId)
    {
        return Idea::joinReactionCounterOfType('Like')
        ->joinReactionCounterOfType('Dislike')
        ->joinReactionTotal()->findOrFail($ideaId);
    }

    public function create(array $data)
    {
       $idea = Idea::create([
            'image'   => 'test',
            'title'   => $data['title'],
            'text'    => $data['text'],
            'user_id' => Auth::user()->id ?? $data['user_id']
        ]);
        if (isset($data['image'])) 
        {
            $file = Storage::putFile('public/'.$idea->id, $data['image']);
        } 
        else
        {
            $file = 'public/gazprom.jpg';
        }

        $idea->update([
            'image' => $file,
        ]);

        return $idea;
    }

    public function delete(int $ideaId)
    {
        $idea = $this->find($ideaId);
        return $idea->delete();
    }

    public function update(int $ideaId, array $data)
    {
        $idea = $this->find($ideaId);
        $idea->update($data);
        return $idea;
    }

    public function getPaginated()
    {
        return Idea::latest()->joinReactionCounterOfType('Like')
            ->joinReactionCounterOfType('Dislike')
            ->joinReactionTotal()->paginate();
    }

    public function like(int $id)
    {
        return $this->reaction($id,'Like');
    }

    public function dislike(int $id)
    {
        return $this->reaction($id,'Dislike');

    }


    public function cancel(int $id)
    {
        $user = Auth::user();
        $idea = $this->find($id);
        $this->registerReaction($user, $idea);

        $reacter = $user->getLoveReacter();
        $reactant = $idea->getLoveReactant();
        $isReacted = $reacter->hasReactedTo($reactant);

        if ($isReacted) {
            $likeReactionType = ReactionType::fromName('Like');
            $dislikeReactionType = ReactionType::fromName('Dislike');
            $isReactedLike = $reacter->hasReactedTo($reactant, $likeReactionType);

            if ($isReactedLike) {
                return $reacter->unreactTo($reactant, $likeReactionType);
            }

            return $reacter->unreactTo($reactant, $dislikeReactionType);
        }
        return false;
    }

    private function reaction(int $id, $type)
    {
        $user = Auth::user();
        $idea = $this->find($id);
        $this->registerReaction($user, $idea);
        $reacter = $user->getLoveReacter();
        $reactionType = ReactionType::fromName($type);
        return $reacter->reactTo($idea->getLoveReactant(), $reactionType);
    }

    private function registerReaction(User $user, Idea $idea)
    {
        if ($idea->isNotRegisteredAsLoveReactant()) {
            $idea->registerAsLoveReactant();
        }

        if ($user->isNotRegisteredAsLoveReacter()) {
            $user->registerAsLoveReacter();
        }
    }
}
