<?php

namespace App\Services\Idea\User\Handlers;

use App\User;
use Illuminate\Support\Facades\Auth;

class TagJoinHandler
{
    public  function handle(int $tagId)
    {
        $user = Auth::user();

        // Чтобы не было дубликотов
        $user->tags()->sync($tagId, false);
        return $user;
    }
}
