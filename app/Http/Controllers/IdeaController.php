<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Idea\IdeaService;

class IdeaController extends Controller
{
    /**
     * @var IdeaService
     */
    protected IdeaService $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    public function index()
    {
        return view('idea');
    }
}
