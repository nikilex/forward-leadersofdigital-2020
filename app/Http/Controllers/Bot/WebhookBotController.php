<?php

namespace App\Http\Controllers\Bot;

use App\Http\Controllers\Controller;
use App\Services\Bot\BotService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WebhookBotController extends Controller
{
    /**
     * @var BotService
     */
    protected BotService $botService;

    public function __construct(BotService $botService)
    {
        $this->botService = $botService;
    }

    public function index()
    {
        $this->botService->webhook();
    }

    public function setWebhook(Request $request)
    {
        $response = $this->push('setwebhook', [
        'query'=> [
            'url' => $request->get('uri') .'/'
        ]]);
        return response((string) $response)->header('Content-Type', 'application/json');
    }

    private function push($action, $params)
    {
        $client = new Client([
                                 'base_uri' => 'https://api.telegram.org/bot' . \Telegram::getAccessToken() . '/'
                             ]);
        $result = $client->request('POST', $action, $params);
        return $result->getBody();
    }
}
