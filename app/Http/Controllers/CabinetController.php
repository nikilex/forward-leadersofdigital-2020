<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Idea\IdeaService;
use Illuminate\Support\Facades\Auth;

class CabinetController extends Controller
{
    /**
     * @var IdeaService
     */
    protected IdeaService $ideaService;

    public function __construct(IdeaService $ideaService)
    {
        $this->ideaService = $ideaService;
    }

    public function index()
    {
        $ideas = $this->ideaService->getPaginated()->where('user_id', Auth::id());
        return view('cabinet', compact('ideas'));
    }
}
