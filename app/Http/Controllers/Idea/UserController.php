<?php

namespace App\Http\Controllers\Idea;

use App\Http\Controllers\Controller;
use App\Services\Idea\User\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * @var UserService
     */
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function joinToTag(Request $request)
    {
        $request->validate([
            'id' => ['required', 'integer', 'exists:tags']
        ]);

        return $this->userService->joinToTag($request->id);
    }
}
