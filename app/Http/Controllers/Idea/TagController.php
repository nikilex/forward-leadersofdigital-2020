<?php

namespace App\Http\Controllers\Idea;

use App\Http\Controllers\Controller;
use App\Services\Idea\Tag\TagService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class TagController extends Controller
{
    protected TagService $tagService;

    public function __construct(TagService $tagService)
    {
        $this->tagService = $tagService;
    }

    public function index(Request $request)
    {
        $tags = $this->tagService->getPaginated();
        dd($tags);
    }

    public function find(int $tagId)
    {
        return $this->tagService->findById($tagId);
    }

    public function search(Request $request)
    {
        $request->validate([
           'name' => ['required', 'string', 'max:128']
        ]);

        return $this->tagService->findByName($request->name);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:128']
        ]);

        return $this->tagService->create($request->all());
    }

    public function update(Request $request)
    {
        $request->validate([
            'id'     =>   ['required', 'integer', 'exists:tags,id'],
            'name'  =>   ['required', 'string', 'unique:tags,name', 'max:128',]
        ]);

        return $this->tagService->update($request->id, $request->all());
    }

    public function delete(Request $request)
    {
        $request->validate([
            'id'     =>   ['required', 'integer', 'exists:tags'],
        ]);

        $this->tagService->remove($request->id);
    }
}
