<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'IdeaController@index');

Route::get('cabinet', 'CabinetController@index');

Route::get('/create', 'IdeaController@index');
Route::get('/', 'IndexController@index');
Route::get('/single/{id}', 'SingleController@index');

Route::get('moderator', function () {
    return view('moderator');
});

Route::post('pro', 'IdeaController@index')->name('pro');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Idea')->group(function () {
    Route::get('/tags', 'TagController@index');
    Route::get('/tags/create', 'TagController@store');
    Route::get('/tags/update', 'TagController@update');
    Route::get('/tags/delete', 'TagController@delete');
    Route::get('/tags/search', 'TagController@search');
    Route::get('/tags/join', 'UserController@joinToTag');
    Route::get('/tags/{tagId}', 'TagController@find');

    Route::get('/idea/', 'IdeaController@index');
    Route::get('/idea/create', 'IdeaController@store');
    Route::post('/idea/like', 'IdeaController@like');
    Route::post('/idea/dislike', 'IdeaController@dislike');
    Route::get('/idea/cancel', 'IdeaController@cancel');

    Route::post('/idea/create', 'IdeaController@store');
});
