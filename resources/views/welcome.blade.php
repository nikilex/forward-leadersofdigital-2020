@extends('layouts.app')

@section('content')



@foreach ($ideas as $key => $idea)
<div class="row post shadow-sm {{ ($key != '0') ? 'mt-5' : '' }} ">
    <div class="col">
        <div class="row p-2">
            <div class="col text-left">
                <span style="font-size: 12px; color: gray">Никитин Алексей разработчики 12 июня 2020 13:55</span>
                 <a href="/single/{{ $idea->id }}" class="title"><h4>  {{ $idea->title }}</h4></a> 
                <p>{!! $idea->text !!}</p>
            </div>
        </div>
        <div class="row">
            <div class="col p-0 mt-2">
                <img class="img-fluid" src="{{ Storage::url($idea->image) }}" alt="">
            </div>
        </div>
        <div class="row p-2">
            <div class="col text-left">
                <span class="vote" onclick="like({{ $idea->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-up">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="4 13 10 7 16 13"></polyline>
                    </svg>
                </span>
                +<span class='rating' id="rating-{{ $idea->id }}">{{ $idea->reaction_like_count }}</span>
                <span class="vote" onclick="dislike({{ $idea->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-down">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="16 7 10 13 4 7"></polyline>
                    </svg>
                </span>
            </div>
            <div class="col text-right" >
                <svg width="22" height="22" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="commenting">
                    <polygon fill="none" stroke="#000" points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                    <circle cx="10" cy="8" r="1"></circle>
                    <circle cx="6" cy="8" r="1"></circle>
                    <circle cx="14" cy="8" r="1"></circle>
                </svg>
                0
            </div>
        </div>
    </div>
</div>
@endforeach

<!-- <div class="row post shadow-sm">
    <div class="col">
        <div class="row p-2">
            <div class="col text-left">
                <span style="font-size: 12px; color: gray">Никитин Алексей разработчики 12 июня 2020 13:55</span>
                <h4>Добавим в мобильное приложение рейтинговую систему?</h4>
                <p>Хотелось бы видеть в нашем приложении рейтинговую систему,
                    которая позволила бы оценивать клиентов и выдавать большой процент по кешбэку.</p>
            </div>
        </div>
        <div class="row">
            <div class="col p-0 mt-2">
                <img class="img-fluid" src="src/img/rating.jpg" alt="">
            </div>
        </div>
        <div class="row p-2">
            <div class="col text-left">
                <span class="vote" onclick="rating(1,'up')">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-up">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="4 13 10 7 16 13"></polyline>
                    </svg>
                </span>
                +<span class='rating' id="rating-1">25</span>
                <span class="vote" onclick="rating(1,'down')">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-down">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="16 7 10 13 4 7"></polyline>
                    </svg>
                </span>
            </div>
            <div class="col text-right" >
                <svg width="22" height="22" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="commenting">
                    <polygon fill="none" stroke="#000" points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                    <circle cx="10" cy="8" r="1"></circle>
                    <circle cx="6" cy="8" r="1"></circle>
                    <circle cx="14" cy="8" r="1"></circle>
                </svg>
                0
            </div>
        </div>
    </div>
</div>
<div class="row post shadow-sm mt-5">
    <div class="col">
        <div class="row p-2">
            <div class="col text-left">
                <span style="font-size: 12px; color: gray">Автор категория 12 июня 2020 13:55</span>
                <h4>Факты и мифы о пищевых добавках </h4>
                <p>Почему говорится, что пищевые добавки безопасны, и когда мы читаем о некоторых красителях,
                    оказывается, что они оказывают плохое влияние, например, на концентрацию у детей - не могут
                    ли они быть заменены другими красящими ингредиентами в такой ситуац</p>
            </div>
        </div>
        <div class="row">
            <div class="col p-0 mt-2">
                <img class="img-fluid" src="src/img/i-mify-o-pischevyh-dobavkah-photo-big.jpg" alt="">
            </div>
        </div>
        <div class="row p-2">
            <div class="col text-left">
                <span href="" class="vote" onclick="rating(2,'up')">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-up">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="4 13 10 7 16 13"></polyline>
                    </svg>
                </span>
                +<span class='rating' id="rating-2">25</span>
                <span class="vote" onclick="rating(2,'down')">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-down">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="16 7 10 13 4 7"></polyline>
                    </svg>
                </span>
            </div>
            <div class="col text-right">
                <svg width="22" height="22" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="commenting">
                    <polygon fill="none" stroke="#000" points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                    <circle cx="10" cy="8" r="1"></circle>
                    <circle cx="6" cy="8" r="1"></circle>
                    <circle cx="14" cy="8" r="1"></circle>
                </svg>
                0
            </div>
        </div>
    </div>
</div> -->
<!-- <div class="row pagination mt-3">
    <p>1 2 3 4 следущая</p>
</div> -->
@endsection