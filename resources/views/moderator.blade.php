@extends('layouts.app')

@section('content')
<div class="row post shadow-sm p-3">
    <div class="col">
        <div class="row">
            <div class="col-3">
                <img src="src/img/moderator.jpg" class=" avatar" alt="">
            </div>
            <div class="col align-self-end">
                <h5>Егор Богданов</h5>
                <span>Модератор</span>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <p>Проверил идей <span style="color:blue;">784,</span> одобрил <span style="color:green;">236</span></p>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <h5>6790</h5>
                <span>рейтинг</span>
            </div>
            <div class="col">
                <h5>589</h5>
                <span>комментариев</span>
            </div>
            <div class="col">
                <h5>784</h5>
                <span>идей</span>
            </div>
            <div class="col">
                <h5>236</h5>
                <span>в топе</span>
            </div>
        </div>
    </div>
</div>

<div class="row post shadow-sm mt-5 p-2 mb-5">
    <div class="col">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Свежие идеи</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Топовые идеи на модерацию</a>
            </li>

        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <ul class="list-group list-group-flush p-3">
                    <li class="list-group-item">Новая идея 1 | рейтинг: <span style="color:red;">+15</span></li>
                    <li class="list-group-item">Новая идея 2 | рейтинг: <span style="color:green;">+51</span></li>
                    <li class="list-group-item">Новая идея 3 | рейтинг: <span style="color:green;">+75</span></li>
                    <li class="list-group-item">Новая идея 4 | рейтинг: <span style="color:red;">+49</span></li>
                    <li class="list-group-item">Новая идея 5 | рейтинг: <span style="color:green;">+156</span></li>
                </ul>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <ul class="list-group list-group-flush p-3">
                    <li class="list-group-item">Топовая идея 1 | рейтинг: <span style="color:green;">+1578</span></li>
                    <li class="list-group-item">Топовая идея 2 | рейтинг: <span style="color:green;">+4515</span></li>
                    <li class="list-group-item">Топовая идея 3 | рейтинг: <span style="color:green;">+1495</span></li>
                    <li class="list-group-item">Топовая идея 4 | рейтинг: <span style="color:green;">+7217</span></li>
                    <li class="list-group-item">Топовая идея 5 | рейтинг: <span style="color:yellow;">+10563</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection