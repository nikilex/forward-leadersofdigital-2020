<nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top shadow-sm">
    <a class="navbar-brand" href="/"><img src="{{ asset('src/img/gazprombank.png')}}" class="logo" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Идеи <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/cabinet">Кабинет</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/moderator">Кабинет модератора</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Поиск" aria-label="Search">
            <button class="btn btn-outline-primary rounded-0 my-2 my-sm-0" type="submit">Поиск</button>
        </form>
    </div>
</nav>