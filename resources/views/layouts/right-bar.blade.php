@guest

<div class="row right-first border rigth-bar-item p-3">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="row">
            <div class="col">
                <h5>ВОЙТИ</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <label class="sr-only" for="email">E-mail</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="login-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                    </div>
                    <input type="text" class="form-control login-input @error('email') is-invalid @enderror" value="{{ old('email') }}" required name="email" id="email" placeholder="E-mail">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-12">
                <label class="sr-only" for="password">Пароль</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="login-icon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    </div>
                    <input type="password" class="form-control login-input @error('password') is-invalid @enderror" name="password" id="password" required placeholder="Пароль">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">Запомнить меня
                        @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">Забыли пароль?</a>
                        @endif
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <button type="submit" class="btn btn-primary rounded-0">Войти</button>
            </div>
            <div class="col">
                <a href="{{ route('register') }}" class="btn btn-light rounded-0">Регистрация</a>
            </div>
        </div>
    </form>
</div>

@else
<div class="row right-first rigth-bar-item p-3">
    <div class="col">
    <h5>Моё дерево</h5>
                <img src="{{ asset('src/img/tree.jpg') }}" class="img-fluid" alt="">
    </div>
</div>

<div class="row mt-5 rigth-bar-item p-3">
    <div class="col">
        <div class="row p-2 ">
            <div class="col-3">
                <img src="{{ asset('src/img/avatar.jpg')}}" class="avatar-mini" alt="">
            </div>
            <div class="col">
                 <a href="/cabinet" class="link-cabinet"><h5 class="link-cabinet">{{ Auth::user()->name }}</h5></a> 
                <span>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Выйти') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </span>
            </div>
        </div>
        <div class="row mt-3 ml-2">
            <div class="col">
                <h5>6790</h5>
                <span>рейтинг</span>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"> <a href="">Мои бонусы</a> </li>
                    <li class="list-group-item"> <a href="">Ответы</a> </li>
                    <li class="list-group-item"><a href="">Комментарии</a></li>
                    <li class="list-group-item"><a href="">Сообщения</a></li>
                    <li class="list-group-item"><a href="">Оценки</a></li>
                </ul>
                
            </div>
        </div>
    </div>
</div>

<div class="row right-first mt-3 p-0">
    <div class="col p-0">
        <a href="/create" class="btn btn-primary btn-lg btn-block rounded-0">+ Добавить идею</a>
    </div>
</div>
@endguest