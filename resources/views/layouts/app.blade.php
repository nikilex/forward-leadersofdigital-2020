<!doctype html>
<html lang="ru">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('src/libs/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('src/css/style.css') }}">

    @yield('head')

    <title>Цифровой прорыв</title>
</head>

<body>

    @include('layouts.header')
    <div class="container-my">
        <div class="row justify-content-between">

            <div class="col col-md-2 left-bar d-none d-md-block">
                @include('layouts.left-menu')
            </div>

            <div class="col col-md-6 main-bar">
                @yield('content')
            </div>

            <div class="col col-md-3 d-none d-md-block">
                @include('layouts.right-bar')
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('src/libs/jquery/jquery-3.5.1.slim.min.js')}}"></script>
    <script src="{{ asset('src/libs/popper/popper.min.js')}}"></script>
    <script src="{{ asset('src/libs/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('src/libs/fontawesome/e9fbff9247.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js" integrity="sha256-T/f7Sju1ZfNNfBh7skWn0idlCBcI3RwdLSS4/I7NQKQ=" crossorigin="anonymous"></script>

    <script>
        function rating(id, type) {
            var vote = $('#rating-' + id);
            var number = parseInt(vote.html());
            var plus = number + 1;
            var minus = number - 1;

            //vote.html(number++)
            if (type == 'up') {
                vote.html(plus)
            } else {
                vote.html(minus)
            }

            axios
                .post('/idea/like', {
                    id: id
                })
                .then(function() {
                    console.log('Запрос прошел успешно');
                })
        }
        function like(id){
            var vote = $('#rating-' + id);
            var number = parseInt(vote.html());
            var plus = number + 1;

            axios
                .post('/idea/like', {
                    id: id
                })
                .then(function(res) {
                    vote.html(plus);
                    console.log(res);
                })
        }
        function dislike(id){
            var vote = $('#rating-' + id);
            var number = parseInt(vote.html());
            var minus = number - 1;

            axios
                .post('/idea/dislike', {
                    id: id
                })
                .then(function(res) {
                    vote.html(minus);
                    console.log(res);
                })
        }
    </script>
    @yield('footer')
</body>

</html>