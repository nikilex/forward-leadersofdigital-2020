<nav class="nav flex-column">
    <a class="nav-item p-2 left-bar-link" href="/cabinet">
        <i class="fa fa-diamond left-bar-icon" aria-hidden="true"></i>
        <span class="left-bar-name">Мои идеи</span>
    </a>
    <a class="nav-item p-2 left-bar-link" href="#">
        <i class="fa fa-envelope-o left-bar-icon" aria-hidden="true"></i>
        <span class="left-bar-name">Сообщения</span>
    </a>
    <a class="nav-item p-2 left-bar-link" href="#">
        <i class="fa fa-star-o left-bar-icon" aria-hidden="true"></i>
        <span class="left-bar-name">Закладки</span>
    </a>
</nav>