@extends('layouts.app')

@section('head')
<link href="src/libs/summernote/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
<form action="/idea/create" method="post" enctype="multipart/form-data">
@csrf
    <div class="row post shadow-sm p-3">
        <div class="col">
            <div class="row">
                <div class="col text-center">
                    <h5>Добавить идею</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="nameIdea" class=" col-form-label">Название</label>
                        <input type="text" class="form-control" id="nameIdea" name="title" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="tags" class="col-form-label">Тэг</label>
                        <select class="form-control" id="tags" name="tags">
                            <option>Разработчики</option>
                            <option>Менеджеры</option>
                            <option>Отдел продаж</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="imageIdea">Изображение</label>
                        <input type="file" class="form-control-file" id="imageIdea" name="image" >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <form method="post">
                            <label for="summernote">Описание идеи</label>
                            <textarea id="summernote" name="text" required></textarea>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col"></div>
            </div>
            <div class="row">
                <div class="col-4">
                    <button type="submit" class="btn btn-primary rounded-0">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('footer')
<script src="src/libs/summernote/summernote.min.js"></script>
<script src="src/libs/summernote/lang/summernote-ru-RU.min.js"></script>

<script>
    $(document).ready(function() {
        $('#summernote').summernote();
    });
</script>
@endsection