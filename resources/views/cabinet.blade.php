@extends('layouts.app')

@section('content')
<div class="row post shadow-sm p-3">
    <div class="col">
        <div class="row">
            <div class="col col-xl-3">
                <img src="{{ asset('src/img/avatar.jpg')}}" class=" avatar" alt="">
            </div>
            <div class="col align-self-end">
                <h5>Никитин Алексей</h5>
                <span>Разработчик</span>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <p>Подал идей <span style="color:blue;">784,</span> одобрили <span style="color:green;">236</span></p>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col">
                <h5>6790</h5>
                <span>рейтинг</span>
            </div>
            <div class="col">
                <h5>589</h5>
                <span>комментариев</span>
            </div>
            <div class="col">
                <h5>236</h5>
                <span>идей в топе</span>
            </div>
        </div>
    </div>
</div>

<div class="row post shadow-sm mt-5 p-2">
    <div class="col">
        <div class="row">
            <div class="col">
                <h5>Награды</h5>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-3">
                <img src="src/img/topmanager.png" class="bonus" alt="">
            </div>
            <div class="col-3">
                <img src="src/img/powerbank.png" class="bonus" alt="">
            </div>
            <div class="col-3">
                <img src="src/img/otgul.png" class="bonus" alt="">
            </div>
        </div>
    </div>
</div>
<div class="row right-first rigth-bar-item p-3 d-md-none d-block">
    <div class="col">
    <h5>Моё дерево</h5>
                <img src="{{ asset('src/img/tree.jpg') }}" class="img-fluid" alt="">
    </div>
</div>
@foreach ($ideas as $key => $idea)
<div class="row post shadow-sm mt-5">
    <div class="col">
        <div class="row p-2">
            <div class="col text-left">
                <span style="font-size: 12px; color: gray">Никитин Алексей разработчики 12 июня 2020 13:55</span>
                 <a href="/single/{{ $idea->id }}" class="title"><h4>  {{ $idea->title }}</h4></a> 
                <p>{!! $idea->text !!}</p>
            </div>
        </div>
        <div class="row p-2">
            <div class="col text-left">
                <span class="vote" onclick="like({{ $idea->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-up">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="4 13 10 7 16 13"></polyline>
                    </svg>
                </span>
                +<span class='rating' id="rating-{{ $idea->id }}">{{ $idea->reaction_like_count }}</span>
                <span class="vote" onclick="dislike({{ $idea->id }})">
                    <svg width="28" height="28" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="chevron-down">
                        <polyline fill="none" stroke="#000" stroke-width="1.03" points="16 7 10 13 4 7"></polyline>
                    </svg>
                </span>
            </div>
            <div class="col text-right" >
                <svg width="22" height="22" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="commenting">
                    <polygon fill="none" stroke="#000" points="1.5,1.5 18.5,1.5 18.5,13.5 10.5,13.5 6.5,17.5 6.5,13.5 1.5,13.5"></polygon>
                    <circle cx="10" cy="8" r="1"></circle>
                    <circle cx="6" cy="8" r="1"></circle>
                    <circle cx="14" cy="8" r="1"></circle>
                </svg>
                0
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection